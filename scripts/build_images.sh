#!/bin/sh

bold=`tput bold`
fawn=$(tput setaf 3); beige="$fawn"
yellow="$bold$fawn"
reset=`tput sgr0`

#if [ "$EUID" != 0 ]; then
#  if ! id -nG "$USER" | grep -qw docker; then
#    echo "${red}ERROR${reset}: The current user must have docker permissions"
#    exit 1
#  fi
#fi

if [ ! -f OpscDockerfile ]; then
  echo "Please run this command in the folder where the Dockerfile's reside"
  exit 1
fi

export $(cat .env | grep -v ^# | xargs)

echo "${yellow}Warning${reset}: This will remove and recreate two images:"
echo "  ${bold}${DOCKER_REGISTRY}/dse-opscenter:latest${reset}"
echo "  ${bold}${DOCKER_REGISTRY}/dse:latest${reset}"
echo "Please make sure those images are no longer in use"

read -p "Proceed: y/n `echo '\n> '`" yn
if [ "$yn" != "y" -a "$yn" != "Y" ]; then
  exit 0
fi

echo "Building OpsCenter image named: ${DOCKER_REGISTRY}/dse-opscenter:latest"
docker rmi ${namespace}/dse-opscenter:latest
docker build . -t ${DOCKER_REGISTRY}/dse-opscenter:latest -f ./OpscDockerfile

echo "Building DSE image named: ${DOCKER_REGISTRY}/dse:latest"
docker rmi ${DOCKER_REGISTRY}/dse:latest
docker build . -t ${DOCKER_REGISTRY}/dse:latest
