#!/bin/bash

# export CLUSTER_NAME=Test_Cluster; sudo scripts/start_docker_compose_cluster.sh

bold=`tput bold`
red=`tput setaf 1`
green=`tput setaf 2`
reset=`tput sgr0`
DIR="$PWD"

if [ ! -f OpscDockerfile ]; then
  echo "Please run this command in the folder where the Dockerfile's reside"
  exit 1
fi

require_root() {
  if [ "$EUID" -ne 0 ]; then
    echo "${red}ERROR${reset}: You must execute as root"
    exit 1
  fi
}

export $(cat .env | grep -v ^# | xargs)

usage() {
  echo ""
  echo "Usage: start_docker_compose_cluster.sh [OPTIONS]"
  echo ""
  echo "Options"
  echo " -n, --nodes     Number of DSE nodes to run (default 1)"
  echo " --dse-image     DSE image name (default \"${DOCKER_REGISTRY}/dse:latest\")"
  echo " --opsc-image    OpsCenter image name (default \"${DOCKER_REGISTRY}/dse-opscenter:latest\")"
  echo " -u, --user      Compose User name (default \"dse\")"
  echo " --cluster-name  Cluster name (default \"Test_Cluster\")"
  echo " --no-opsc       Don't run opscenter image"
  exit 1
}

while test $# -gt 0; do
  case "$1" in
    -h|--help)
      usage
      ;;
    -n|--nodes)
      shift
      if test $# -gt 0; then
        export NUM_NODES=$1
      else
        echo "no node specified"
        exit 1
      fi
      shift
      ;;
    --dse-image)
      shift
      if test $# -gt 0; then
        DSE_IMAGE=$1
      else
        echo "no dse image specified"
        exit 1
      fi
      shift
      ;;
    --opsc-image)
      shift
      if test $# -gt 0; then
        OPSC_IMAGE=$1
      else
        echo "no opscenter image specified"
        exit 1
      fi
      shift
      ;;
    -u|--user)
      shift
      if test $# -gt 0; then
        COMPOSE_USER=$1
      else
        echo "no user specified"
        exit 1
      fi
      shift
      ;;
    --no-opsc)
      OPSC_IMAGE="none"
      shift
      ;;
    --cluster-name)
      shift
      if test $# -gt 0; then
        CLUSTER_NAME=$1
      else
        echo "no cluster name specified"
        exit 1
      fi
      shift
      ;;
    *)
      break
      ;;
  esac
done

require_root

if ! docker-compose -v >/dev/null 2>&1; then
  echo "${red}ERROR${reset}: You don't have docker-compose installed"
  echo "https://docs.docker.com/compose/install/"
  exit 1
fi

[ -z "$NUM_NODES" ] && NUM_NODES=1
[ -z "$CLUSTER_NAME" ] && CLUSTER_NAME="Test_Cluster"
[ -z "$COMPOSE_USER" ] && COMPOSE_USER="dse"
[ -z "$DSE_IMAGE" ] && DSE_IMAGE="${DOCKER_REGISTRY}/dse:latest"
[ -z "$OPSC_IMAGE" ] && OPSC_IMAGE="${DOCKER_REGISTRY}/dse-opscenter:latest"

if ! id -u $COMPOSE_USER >/dev/null 2>&1; then
  echo "Adding user ${COMPOSE_USER}"
  useradd -m -s /bin/bash $COMPOSE_USER
fi

COMPOSE_DIR=$(grep $COMPOSE_USER /etc/passwd | cut -f6 -d ":")

if ! id -nG "$COMPOSE_USER" | grep -qw docker; then
  echo "Adding docker group to user ${COMPOSE_USER}"
  usermod -aG docker $COMPOSE_USER
fi

if [ -f "$COMPOSE_DIR/docker-compose.yml" ]; then
  echo "${red}ERROR${reset}: Already have a docker-compose.yml file"
  exit 1
fi

cd $COMPOSE_DIR

echo "Creating docker-compose.yml file"
cat <<EOT >> $COMPOSE_DIR/docker-compose.yml
version: "2"
networks:
  default:
    driver: bridge
services:
EOT
chown $COMPOSE_USER:$COMPOSE_USER $COMPOSE_DIR/docker-compose.yml

if [ "$OPSC_IMAGE" != "" -a "$OPSC_IMAGE" != "none" ]; then
  echo "Adding opscenter to compose"
  cat <<EOT >> $COMPOSE_DIR/docker-compose.yml
  opscenter:
    container_name: ${COMPOSE_USER}_opscenter
    image: ${OPSC_IMAGE}
    restart: always
    networks:
      - default
    ports:
      - 8888:8888
EOT

  echo "Running node opscenter"
  su $COMPOSE_USER -c "docker-compose up -d opscenter"
  STOMP_INTERFACE=`docker exec ${COMPOSE_USER}_opscenter hostname -I | cut -d " " -f1`
fi

echo "Adding node_1 to compose"
cat <<EOT >> $COMPOSE_DIR/docker-compose.yml
  node_1:
    container_name: ${COMPOSE_USER}_node_1
    image: ${DSE_IMAGE}
    restart: always
    networks:
      - default
    environment:
      - STOMP_INTERFACE=${STOMP_INTERFACE}
      - CLUSTER_NAME=${CLUSTER_NAME}
    ports:
      - 9042:9042
      - 7199:7199
EOT

if [ "$OPSC_IMAGE" != "" -a "$OPSC_IMAGE" != "none" ]; then
  cat <<EOT >> $COMPOSE_DIR/docker-compose.yml
    links:
      - opscenter
EOT
fi

echo "Running node_1"
su $COMPOSE_USER -c "docker-compose up -d node_1"

if [ "$OPSC_IMAGE" != "" -a "$OPSC_IMAGE" != "none" ]; then
  echo ""
  echo "${green}Seed node IP address for OpsCenter cluster lookup${reset}"
  echo "${bold}${green} $(docker inspect --format '{{ .NetworkSettings.Networks.${COMPOSE_USER}_default.IPAddress }}' ${COMPOSE_USER}_node_1)${reset}"
fi

SEEDS=$(docker exec ${COMPOSE_USER}_node_1 hostname -I)

let n=1

while [ $n != $NUM_NODES ]; do
  let n=n+1

  echo "Adding node_${n} to compose"
  cat <<EOT >> $COMPOSE_DIR/docker-compose.yml
  node_${n}:
    image: ${DSE_IMAGE}
    container_name: ${COMPOSE_USER}_node_${n}
    restart: always
    networks:
      - default
    environment:
      - STOMP_INTERFACE=${STOMP_INTERFACE}
      - CLUSTER_NAME=${CLUSTER_NAME}
      - SEEDS=${SEEDS}
EOT

  if [ "$OPSC_IMAGE" != "" -a "$OPSC_IMAGE" != "none" ]; then
  cat <<EOT >> $COMPOSE_DIR/docker-compose.yml
    links:
      - opscenter
EOT
  fi

  echo "Running node_${n}"
  su $COMPOSE_USER -c "docker-compose up -d node_${n}"
done
