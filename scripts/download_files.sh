#!/bin/sh

if [ ! -f OpscDockerfile ]; then
  echo "Please run this command in the folder where the Dockerfile's reside"
  exit 1
fi

export $(cat .env | grep -v ^# | xargs)

mkdir -p files

echo "Downloading DataStax Enterprise"
[ -e files/dse-bin.tar.gz ] \
  || curl --user $DSE_USER:$DSE_PASS -L http://downloads.datastax.com/enterprise/dse-5.0.6-bin.tar.gz > files/dse-bin.tar.gz

echo "Downloading DataStax Agent"
[ -e files/datastax-agent_all.deb ] \
  || curl --user $DSE_USER:$DSE_PASS -L http://debian.datastax.com/enterprise/pool/datastax-agent_6.0.8_all.deb > files/datastax-agent_all.deb

echo "Downloading DataStax OpsCenter"
[ -e files/opscenter.tar.gz ] \
  || curl --user $DSE_USER:$DSE_PASS -L http://downloads.datastax.com/enterprise/opscenter-6.0.8.tar.gz > files/opscenter.tar.gz
