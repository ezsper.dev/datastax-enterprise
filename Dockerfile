# Provided without any warranty, these files are intended
# to accompany the whitepaper about DSE on Docker and are
# not intended for production and are not actively maintained.

# Loosely based on docker-cassandra by the fine folk at Spotify
# -- https://github.com/spotify/docker-cassandra/
# Loosely based on cassandra-docker by the one and only Al Tobey
# -- https://github.com/tobert/cassandra-docker/

# base yourself on any ubuntu 14.04 image containing JDK8
# official Docker Java images are distributed with OpenJDK
# Datastax certifies its product releases specifically
# on the Oracle/Sun JVM, so YMMV with OpenJDK

FROM nimmis/java:oracle-8-jdk

RUN export DEBIAN_FRONTEND=noninteractive && \
    apt-get update && \
    apt-get -y install adduser \
    curl \
    lsb-base \
    procps \
    sudo \
    zlib1g \
    gzip \
    python \
    sysstat \
    openssh-server \
    ntp bash tree && \
    rm -rf /var/lib/apt/lists/*

RUN wget http://launchpadlibrarian.net/109052632/python-support_1.0.15_all.deb -P /tmp && \
    dpkg -i /tmp/python-support_1.0.15_all.deb && \
    rm /tmp/python-support_1.0.15_all.deb

# grab gosu for easy step-down from root
RUN curl -o /bin/gosu -SkL "https://github.com/tianon/gosu/releases/download/1.4/gosu-$(dpkg --print-architecture)" \
    && chmod +x /bin/gosu

RUN mkdir /var/run/sshd
RUN echo "    IdentityFile ~/.ssh/id_rsa" >> /etc/ssh/ssh_config
RUN sed 's@session\s*required\s*pam_loginuid.so@session optional pam_loginuid.so@g' -i /etc/pam.d/sshd

ENV NOTVISIBLE "in users profile"
RUN echo "export VISIBLE=now" >> /etc/profile

ENV DSE_HOME /opt/dse

# keep data here
VOLUME /data

# and logs here
VOLUME /logs

COPY files/dse-bin.tar.gz* /opt/
COPY files/datastax-agent_all.deb* /tmp/

# DataStax credentials

ARG DSE_USER ""
ARG DSE_PASS ""

# Download DataStax Enterprise directly from their storage

RUN [ -e /opt/dse-bin.tar.gz ] \
    || curl --user $DSE_USER:$DSE_PASS -L http://downloads.datastax.com/enterprise/dse-5.1.2-bin.tar.gz > /opt/dse-bin.tar.gz

RUN tar -xzf /opt/dse-bin.tar.gz -C /opt/ \
    && rm /opt/dse-bin.tar.gz

RUN ln -s /opt/dse* $DSE_HOME

# create unique SSH keys for external authentication with cassandra user

RUN mkdir -p $DSE_HOME/.ssh && \
    ssh-keygen -q -t rsa -N '' -f $DSE_HOME/.ssh/id_rsa -C 'cassandra@'`hostname`

VOLUME /opt/dse

RUN [ -e /tmp/datastax-agent_all.deb ] \
    || curl --user $DSE_USER:$DSE_PASS -L http://debian.datastax.com/enterprise/pool/datastax-agent_6.1.2_all.deb > /tmp/datastax-agent_all.deb

# create a dedicated user for running DSE node
RUN groupadd -g 1337 cassandra && \
    useradd -u 1337 -g cassandra -s /bin/bash -d $DSE_HOME cassandra && \
    chown -R cassandra:cassandra /opt/dse*

# install the agent
RUN dpkg -i /tmp/datastax-agent_all.deb

# starting node using custom entrypoint that configures paths, interfaces, etc.
COPY scripts/dse-entrypoint /usr/local/bin/
RUN chmod +x /usr/local/bin/dse-entrypoint
ENTRYPOINT ["/usr/local/bin/dse-entrypoint"]

# Running any other DSE/C* command should be done on behalf dse user
# Perform that using a generic command laucher
COPY scripts/dse-cmd-launcher /usr/local/bin/
RUN chmod +x /usr/local/bin/dse-cmd-launcher

# link dse commands to the launcher
RUN for cmd in cqlsh dsetool nodetool dse cassandra-stress; do \
        ln -sf /usr/local/bin/dse-cmd-launcher /usr/local/bin/$cmd ; \
    done

# the detailed list of ports
# http://docs.datastax.com/en/datastax_enterprise/5.0/datastax_enterprise/sec/secConfFirePort.html

# SSH
EXPOSE 22

# Cassandra
EXPOSE 7199 7000 9042 9160

# Solr
EXPOSE 8983 8984

# Spark
EXPOSE 4040 7080 7081 7077

# Hadoop
EXPOSE 8012 50030 50060 9290

# Hive/Shark
EXPOSE 10000

# Graph
